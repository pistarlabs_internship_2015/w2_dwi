<?php

//dwi

//API Url
$url = 'http://192.168.0.59:1300';

//Initiate cURL.
$ch = curl_init($url);

//The JSON data.
$jsonData = array(
    'app_key' => '3456789012',
  	'device_token' => '1001',
  	'phone_number' => '081234567890',
  	'message' => 'Intern : Hi there!'
);
echo "Success json<br/>";

//Encode the array into JSON.
$jsonDataEncoded = json_encode($jsonData);
echo "Success encoded<br/>";

//Tell cURL that we want to send a POST request.
curl_setopt($ch, CURLOPT_POST, 1);

//Attach our encoded JSON string to the POST fields.
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

//Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

//Execute the request
$result = curl_exec($ch);
echo "<br/>Success post<br/>";

?>